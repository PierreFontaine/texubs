# texubs

## Abstract

Ce package permet de créer des présentations beamer en accord avec la charte graphique UBS.

## Contenu

### Logos

logos de l'UBS

### Ucolors

couleurs définies par la charte UBS

### themeBeamer

theme qui correspond à la charte UBS pour la création de présentations.
Nom du thème `UBS2019`